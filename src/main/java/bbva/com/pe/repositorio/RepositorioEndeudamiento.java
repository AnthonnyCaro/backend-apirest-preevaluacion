package bbva.com.pe.repositorio;

import bbva.com.pe.modelo.ModeloEndeudamiento;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioEndeudamiento extends MongoRepository<ModeloEndeudamiento, String> {
}
