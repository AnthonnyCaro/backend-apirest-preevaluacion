package bbva.com.pe.servicio;

import bbva.com.pe.modelo.ModeloEndeudamiento;
import org.springframework.data.domain.Page;


public interface EndeudamientoService {

    public Page<ModeloEndeudamiento> obtenerEndeudamientos(int pagina, int tamanio);

    public void agregarEndeudamiento(ModeloEndeudamiento endeudamiento);

    public void actualizarEndeudamiento(String codigo, ModeloEndeudamiento endeudamiento);

    public void eliminarEndeudamiento(String codigo);
    
    public ModeloEndeudamiento obtenerEndeudamientosById(String codigo);
    
}