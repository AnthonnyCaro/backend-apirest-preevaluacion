package bbva.com.pe.controlador;

import bbva.com.pe.configuraciones.RutasApi;
import bbva.com.pe.modelo.ModeloEndeudamiento;
import bbva.com.pe.servicio.EndeudamientoService;
import bbva.com.pe.servicio.EndeudamientoServiceResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(RutasApi.BASE)
public class ControladorEndeudamiento {

    @Autowired
    EndeudamientoService endeudamientoService;

    @GetMapping(RutasApi.ENDEUDAMIENTOS)
    public Page<ModeloEndeudamiento> obtenerEndeudamientos(
    		@RequestParam(required = false) Integer pagina, 
    		@RequestParam(required = false) Integer tamanio) {
    	
		if (pagina == null || pagina < 0)
			pagina = 0;
		if (tamanio == null || tamanio < 20)
			tamanio = 20;
		if (tamanio > 100)
			tamanio = 100;
		
        return this.endeudamientoService.obtenerEndeudamientos(pagina,tamanio);
    }
    
    // GetID
    @GetMapping(RutasApi.ENDEUDAMIENTOS_ID)
    public ModeloEndeudamiento obtenerEndeudamientosById (@PathVariable(name = "id") String id){
    	return this.endeudamientoService.obtenerEndeudamientosById(id);
    }

    // POST : Insertar el endeudamiento
    @PostMapping(RutasApi.ENDEUDAMIENTOS)
    public void agregarEndeudamiento(@RequestBody ModeloEndeudamiento endeudamiento) {
    	this.endeudamientoService.agregarEndeudamiento(endeudamiento);
    }

    // PUT
    @PutMapping(RutasApi.ENDEUDAMIENTOS_ID)
    public void actualizarEndeudamiento(@PathVariable String codigo, @RequestBody ModeloEndeudamiento endeudamiento) {
        this.endeudamientoService.actualizarEndeudamiento(codigo, endeudamiento);
    }

    // DELETE
    @DeleteMapping(RutasApi.ENDEUDAMIENTOS_ID)
    public void eliminarEndeudamiento(@PathVariable String codigo) {
        this.endeudamientoService.eliminarEndeudamiento(codigo);
    }

}
